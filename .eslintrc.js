module.exports = {
    extends: ['plugin:@typescript-eslint/recommended', 'airbnb-typescript'],
    parser: '@typescript-eslint/parser',
    plugins: ['@typescript-eslint', 'prettier', 'react-hooks'],
    parserOptions: {
        project: './tsconfig.json',
    },
    settings: {
        'import/parsers': {
            '@typescript-eslint/parser': ['.ts', '.tsx'],
        },
        'import/resolver': {
            typescript: {},
            node: {
                extensions: ['.js', '.jsx', '.ts', '.tsx'],
                moduleDirectory: ['node_modules', 'src/'],
             }
        },
    },
    rules: {
        'react/jsx-filename-extension': [2, {
            extensions: ['.js', '.jsx', '.ts', '.tsx']
        }],
        '@typescript-eslint/indent': [2, 2],
        'import/extensions': 'off'
    },
};