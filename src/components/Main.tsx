import React, { useState } from 'react';
import './Main.css';
import { Selected } from 'interfaces';
import useApiText from '../queries/use-api-text';
import useTransform from '../hooks/use-transform';
import useChartData from '../hooks/use-chart-data';
import useFilterData from '../hooks/use-filter-data';
import { StateOfProcess, ApiStates } from '../const';
import Chart from './Chart';
import Filter from './Filter';

const URL = process.env.REACT_APP_DATA_URL as string;

const Main = () => {
  const [appliedFilter, setAppliedFilter] = useState<Selected>({
    datasources: [],
    campaign: '',
  });

  const { apiState, apiError, data } = useApiText(URL);

  const {
    adList, transformError, transformState, keyValueDimensions,
  } = useTransform(
    apiState,
    data ?? '',
  );

  const {
    selected, options, setCampaign, setDatasources, setOptions,
  } = useFilterData(
    keyValueDimensions,
    transformState,
  );
  const { nodeList, nodeListState } = useChartData(adList, transformState, appliedFilter);

  const combineLoadState = (loadState: string, adState: string, nodeState: string) => {
    if (!(adState === StateOfProcess.PROCESSED && nodeState === StateOfProcess.PROCESSED)) {
      return loadState;
    }
    return StateOfProcess.PROCESSED;
  };

  switch (combineLoadState(apiState, transformState, nodeListState)) {
    case ApiStates.ERROR || transformError.length > 0:
      return (
        <p>
          ERROR:
          {apiError || transformError || 'General error'}
        </p>
      );
    case ApiStates.SUCCESS:
      return <p>processing...</p>;
    case StateOfProcess.PROCESSED:
      return (
        <div className="Main">
          <div className="filter-wrapper">
            <Filter
              options={options}
              selected={selected}
              setCampaign={setCampaign}
              setDatasources={setDatasources}
              setOptions={setOptions}
            />
            <div className="button-wrapper">
              <button type="button" onClick={() => setAppliedFilter({ ...selected })}>Apply</button>
            </div>
          </div>

          <div className="chart">
            <div className="selection-title">
              <small>
                campaign:
                {` ${appliedFilter.campaign || 'all'} `}
              </small>
              <small>
                | datasources:
                {` ${appliedFilter.datasources.toString() || 'all'} `}
              </small>
            </div>
            <Chart nodes={nodeList} />
          </div>

        </div>
      );
    default:
      return <p>loading...</p>;
  }
};

export default Main;
