import React from 'react';
import Select from 'react-select';
import './Filter.css';

import {
  SelectOption, Options, Selected, OptionUpdateInput,
} from '../interfaces';

interface SelectProps {
  options: Options;
  defaultText?: string;
  selected: Selected;
  setCampaign: (campaign: string) => void;
  setDatasources: (datasources: string[]) => void;
  setOptions: (selected: OptionUpdateInput) => void;
}

const Filter = (selectProps: SelectProps) => {
  const {
    options: { campaignList, datasourceList },
    selected: { campaign, datasources },
    setCampaign,
    setDatasources,
    setOptions,
  } = selectProps;

  const handleCampaignChange = (campaignSelect: SelectOption | null) => {
    setCampaign(campaignSelect !== null ? campaignSelect.value : '');
    setOptions({ campaign: campaignSelect !== null ? campaignSelect.value : '' });
  };

  const handleDataSourceChange = (dsListSelect: SelectOption[]) => {
    if (dsListSelect !== null) {
      setDatasources(Array.from(dsListSelect, (ds) => (ds.value)));
      setOptions({ datasources: Array.from(dsListSelect, (ds) => (ds.value)) });
    } else {
      setDatasources([]);
      setOptions({ datasources: [] });
    }
  };

  return (
    <section className="Filter">
      <div className="filter-section-wrapper">
        <div className="filter-section">
          <p>datasources:</p>
          <Select
            value={datasourceList.filter((obj) => datasources.includes(obj.value))}
            onChange={(option) => handleDataSourceChange(option as SelectOption[])}
            options={datasourceList}
            isMulti
            isClearable
            name="datasources"
            className="basic-select basic-multi-select"
            classNamePrefix="select"
          />
        </div>

        <div className="filter-section">
          <p>campaign:</p>
          <Select
            selected={campaign}
            className="basic-select"
            onChange={(option) => handleCampaignChange(option as SelectOption)}
            classNamePrefix="select"
            options={campaignList}
            name="campaign"
            isSearchable
            isClearable
          />
        </div>
      </div>

    </section>
  );
};
export default Filter;
