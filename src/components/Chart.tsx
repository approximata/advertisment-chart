import React from 'react';
import {
  LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

import { Node } from '../interfaces/index';

interface ChartProps {
  nodes: Node[]
}

const WIDTH = 750;

const Chart = (chartProps: ChartProps) => {
  const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
    'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  const formatDataKey = (date: string) => {
    if (date) {
      return `${date.split('.')[0]}.${monthNames[parseInt(date.split('.')[1], 10) - 1]}`;
    }
    return '';
  };
  const chartSize = {
    width: window.innerWidth - 20 <= WIDTH ? window.innerWidth - 20 : WIDTH,
    height: (window.innerWidth - 20 <= WIDTH ? window.innerWidth - 20 : WIDTH) / 2,
  };

  return (
    <LineChart
      width={chartSize.width}
      height={chartSize.height}
      data={chartProps.nodes}
      margin={{
        top: 5, right: 30, left: 20, bottom: 5,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis
        dataKey="date"
        tickFormatter={formatDataKey}
        tick={{ fontSize: 10, fill: '#F2F2F2' }}
      />
      <YAxis
        yAxisId="left"
        label={{
          value: 'clicks', position: 'insideRight', dy: 10, dx: -60, angle: -90, fill: '#F2F2F2',
        }}
        tick={{ fontSize: 8, fill: '#F2F2F2' }}
      />
      <YAxis
        yAxisId="right"
        label={{
          value: 'impressions', position: 'insideLeft', dy: 10, dx: 80, angle: 90, fill: '#F2F2F2',
        }}
        tick={{ fontSize: 8, fill: '#F2F2F2' }}
        orientation="right"
      />
      <Tooltip labelStyle={{ color: '#262626' }} />
      <Legend />
      <Line yAxisId="left" type="monotone" dataKey="clicks" stroke="#CF5252" activeDot={{ r: 6 }} />
      <Line yAxisId="right" type="monotone" dataKey="impressions" stroke="#4B9999" />
    </LineChart>
  );
};
export default Chart;
