import { OptionTypeBase } from 'react-select';
import { AdColumn } from '../const';

export interface Ad {
  [AdColumn.date]: string;
  [AdColumn.datasource]: string;
  [AdColumn.campaign]: string;
  [AdColumn.clicks]: number;
  [AdColumn.impressions]: number;
}

export interface Node {
  [AdColumn.date]: string;
  [AdColumn.clicks]: number;
  [AdColumn.impressions]: number;
}

export interface Filter {
  selected: Selected;
  options: Options;
  unfilteredOptions: Options;
}

export interface Selected {
  datasources: string[];
  campaign: string;
}

export interface OptionUpdateInput {
  datasources?: string[];
  campaign?: string;
}

export interface SelectOption extends OptionTypeBase {
  value: string;
  label: string;
}

export interface Options {
  datasourceList: SelectOption[];
  campaignList: SelectOption[];
}

export interface KeyValueListPair {
  [key: string]: Set<string>;
}

export interface KeyValueDimensions {
  campaignDsListPair: KeyValueListPair,
  dsCampaignListPair: KeyValueListPair,
}
