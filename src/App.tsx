import React from 'react';
import './App.css';
import Main from './components/Main';

const App = () => (
  <div className="App">
    <h3 className="title">Advertisment Data Diagram</h3>
    <Main />
  </div>
);

export default App;
