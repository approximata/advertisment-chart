export enum ApiStates {
  LOADING = 'LOADING',
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
}

export enum AdColumn {
  date = 'date',
  datasource = 'datasource',
  campaign = 'campaign',
  clicks = 'clicks',
  impressions = 'impressions',
}

export enum StateOfProcess {
  PROCESSING = 'PROCESSING',
  PROCESSED = 'PROCESSED',
  PROCESSED_FAILED = 'PROCESS_FAILED',
}
