import { useState, useEffect } from 'react';
import { ApiStates } from '../const';

interface Response {
  apiState: string;
  data?: string;
  apiError?: string;
}

const useApiText = (url: string): Response => {
  const [data, setData] = useState<Response>({
    apiState: ApiStates.LOADING,
    apiError: '',
    data: '',
  });

  const setPartData = (partialData: Response) => setData({ ...data, ...partialData });

  useEffect(() => {
    setPartData({
      apiState: ApiStates.LOADING,
    });
    fetch(url)
      .then((response) => response.text())
      .then((text) => {
        setPartData({
          apiState: ApiStates.SUCCESS,
          data: text,
        });
      })
      .catch((e) => {
        setPartData({
          apiState: ApiStates.ERROR,
          apiError: `Fetch failed: ${e}`,
        });
      });
  }, [url]);

  return data;
};

export default useApiText;
