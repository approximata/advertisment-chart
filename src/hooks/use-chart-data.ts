import {
  useState,
  useEffect,
} from 'react';
import _ from 'lodash';

import {
  Ad,
  Node,
  Selected,
} from '../interfaces';

import { StateOfProcess } from '../const';

interface ChartDataState {
  nodeList: Node[],
  nodeListState: string
}

interface SumByDate {
  [date: string]: Node;
}

const useChartData = (adList: Ad[], adState: string, appliedFilter: Selected): ChartDataState => {
  const [chartData, setChartData] = useState < ChartDataState >({
    nodeList: [],
    nodeListState: StateOfProcess.PROCESSING,
  });
  useEffect(() => {
    if (adState !== StateOfProcess.PROCESSED) return;
    const filteredAds: Ad[] = _.filter(adList, (ad) => (
      (appliedFilter.datasources.includes(ad.datasource) || appliedFilter.datasources.length === 0)
        && (ad.campaign === appliedFilter.campaign || appliedFilter.campaign.length === 0)
    ));

    const sumByDateCollection = _.reduce(filteredAds, (sum: SumByDate, ad) => {
      const sumByDate = { ...sum };
      const key = ad.date;
      const {
        clicks,
        impressions,
      } = ad;
      if (!sumByDate[key]) {
        sumByDate[key] = {
          date: key,
          clicks,
          impressions,
        };
      } else {
        sumByDate[key].clicks += clicks;
        sumByDate[key].impressions += impressions;
      }
      return sumByDate;
    }, {});

    const newNodeList: Node[] = _.values(sumByDateCollection);
    setChartData({
      ...chartData,
      nodeList: newNodeList,
      nodeListState: StateOfProcess.PROCESSED,
    });
  }, [adState, adList, appliedFilter]);
  return chartData;
};

export default useChartData;
