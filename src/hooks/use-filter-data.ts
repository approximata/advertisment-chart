import {
  useState,
  useEffect,
} from 'react';

import _ from 'lodash';
import {
  Filter, Selected, Options, KeyValueDimensions, OptionUpdateInput, SelectOption,
} from '../interfaces';
import { StateOfProcess } from '../const';

const useFilterData = (
  keyValueString: KeyValueDimensions,
  transformState: string,
) => {
  const initSelected: Selected = {
    datasources: [],
    campaign: '',
  };

  const initOptions: Options = {
    datasourceList: [],
    campaignList: [],
  };

  const [filter, setFilter] = useState<Filter>({
    selected: initSelected,
    options: initOptions,
    unfilteredOptions: initOptions,
  });

  useEffect(() => {
    if (transformState === StateOfProcess.PROCESSED) {
      const { campaignDsListPair, dsCampaignListPair } = keyValueString;
      const newOption = { ...initOptions };
      newOption.campaignList = Object.keys(campaignDsListPair).map(
        (cp) => ({ label: cp, value: cp }),
      );
      newOption.datasourceList = Object.keys(dsCampaignListPair).map(
        (ds) => ({ label: ds, value: ds }),
      );

      setFilter({
        ...filter,
        options: newOption,
        unfilteredOptions: _.cloneDeep(newOption),
      });
    }
  }, [keyValueString, transformState]);

  const setOptions = (optionUpdate: OptionUpdateInput) => {
    const newFilter = { ...filter };
    if (optionUpdate.campaign && optionUpdate.campaign !== '') {
      const newDatasourceList = Array.from(
        keyValueString.campaignDsListPair[optionUpdate.campaign],
      ).map(
        (cp) => ({ label: cp, value: cp }),
      );
      newFilter.options.datasourceList = newDatasourceList;
    } else if (optionUpdate.datasources && optionUpdate.datasources.length > 0) {
      const newCampaignList: SelectOption[] = [];
      optionUpdate.datasources.forEach((datasource) => newCampaignList.push(
        ...Array.from(keyValueString.dsCampaignListPair[datasource]).map(
          (ds) => ({ label: ds, value: ds }),
        ),
      ));
      newFilter.options.campaignList = newCampaignList;
    } else {
      newFilter.options = { ...newFilter.unfilteredOptions };
    }
    setFilter({ ...newFilter });
  };

  const setCampaign = (campaign: string) => {
    const newFilter: Filter = {
      ...filter,
    };
    newFilter.selected.campaign = campaign;
    setFilter({
      ...newFilter,
    });
  };

  const setDatasources = (datasources: string[]) => {
    const newFilter: Filter = {
      ...filter,
    };
    newFilter.selected.datasources = datasources;
    setFilter({
      ...newFilter,
    });
  };

  return {
    ...filter,
    setCampaign,
    setDatasources,
    setOptions,
  };
};

export default useFilterData;
