import { createAdFromColumns } from './use-transform';

describe('createAdFromColumns test', () => {
  it('should transform columns to Ad', () => {
    const line = ['01.01.2019', 'Facebook Ads', 'Like Ads', '274', '1979'];
    const keys = ['date', 'datasource', 'campaign', 'clicks', 'impressions'];
    const result = {
      date: '01.01.2019', datasource: 'Facebook Ads', campaign: 'Like Ads', clicks: 274, impressions: 1979,
    };
    expect(createAdFromColumns(line, keys)).toEqual(result);
  });
});
