import { useState, useEffect } from 'react';
import _ from 'lodash';

import { AdColumn, StateOfProcess, ApiStates } from '../const';
import { Ad, KeyValueDimensions } from '../interfaces';

interface ErrorProperties {
  line?: number;
  message: string;
}

interface AdTransform {
  adList: Ad[];
  keyValueDimensions: KeyValueDimensions,
  failedLines: ErrorProperties[];
  transformError: string;
  transformState: string;
}

export const createAdFromColumns = (columns: string[], keys: string[]): Ad => {
  const ad: Partial<Ad> = {};
  columns.forEach((column, columnIndex) => {
    const key = keys[columnIndex] as keyof Ad;
    if (key === AdColumn.clicks || key === AdColumn.impressions) {
      ad[key] = Object.is(NaN, parseInt(column, 10)) ? 0 : parseInt(column, 10);
    } else {
      ad[key] = column;
    }
  });
  return ad as Ad;
};

const createError = (index: number, errorReson = ''): ErrorProperties => {
  const errorProp: ErrorProperties = {
    line: index,
    message: `Error at line ${index}. ${errorReson}`,
  };
  return errorProp;
};

const useTransform = (apiState: string, data: string): AdTransform => {
  const [adTransform, setAdTransform] = useState<AdTransform>({
    adList: [],
    keyValueDimensions: {
      campaignDsListPair: {},
      dsCampaignListPair: {},
    },
    failedLines: [],
    transformError: '',
    transformState: StateOfProcess.PROCESSING,
  });

  useEffect(() => {
    if (apiState === ApiStates.SUCCESS) {
      try {
        const newAdTransform: AdTransform = _.clone(adTransform);
        const {
          adList,
          failedLines,
          keyValueDimensions:
          {
            campaignDsListPair,
            dsCampaignListPair,
          },
        } = newAdTransform;

        const lines = _.split(data, '\n');

        const keys = lines[0]
          .split(',')
          .map((columnName) => columnName.toLocaleLowerCase());
        const campaignColumnIndex = keys.indexOf('campaign');
        const dataSourceColumnIndex = keys.indexOf('datasource');

        lines.forEach((line, lineIndex) => {
          if (lineIndex !== 0) {
            const columns = line.split(',');
            if (columns.length === keys.length) {
              const ad: Ad = createAdFromColumns(columns, keys);
              adList.push(ad as Ad);
              const campaign = columns[campaignColumnIndex];
              const datasource = columns[dataSourceColumnIndex];
              if (campaignDsListPair[campaign]) campaignDsListPair[campaign].add(datasource);
              else campaignDsListPair[campaign] = new Set().add(datasource) as Set<string>;
              if (dsCampaignListPair[datasource]) dsCampaignListPair[datasource].add(campaign);
              else dsCampaignListPair[datasource] = new Set().add(campaign) as Set<string>;
            } else {
              failedLines.push(createError(lineIndex, 'One or more item is deviate from expected element'));
            }
          }
        });
        setAdTransform({
          ...adTransform,
          adList,
          failedLines,
          keyValueDimensions: { campaignDsListPair, dsCampaignListPair },
          transformState: StateOfProcess.PROCESSED,
        });
      } catch (error) {
        setAdTransform({
          ...adTransform,
          transformError: `Something went wrong :( \n ${error}`,
          transformState: StateOfProcess.PROCESSED_FAILED,
        });
      }
    }
  }, [apiState, data]);
  return adTransform;
};

export default useTransform;
