const express = require('express');
const port = process.env.PORT || 3001;
const path = require('path');
const app = express();

app.use('/', express.static(path.join(__dirname, 'build')));

app.listen(port, (error) => {
    if (error) {
        console.error(error)
    } else {
        console.info('==> 🌎  Listening on port %s.', port)
    }
})
